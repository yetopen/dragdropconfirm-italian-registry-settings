# DragDropConfirm Italian strings

This repo contains the necessary registry keys to make [DragDropConfirm](https://github.com/broken-e/DragDropConfirm) work with Windows in Italian.

Install the program and import this registry file to enable the warning dialog.
